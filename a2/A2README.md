# LIS4930 - Special Topics: Advanced Mobile Web Application Development

## Isaac Bass

### Assignment 2 Requirements:

1. Back-ward engineer tip calculator application
2. Must include:
    * Drop-down number for total number of guests (including yourself): 1-10
    * Drop-down menu for tip percentage (5% increments): 0-25
    * Must add background color(s) or theme

#### README.md file should include the following items:

* Screenshot of running application's **unpopulated** user interface
* Screenshot of running application's **populated** user interface

#### Assignment Screenshots:

|*Screenshot of running application's unpopulated user interface*|*Screenshot of running application's populated user interface*|
|-----------------------------------------------------------------|--------------------------------------------------------------|
|![Tip calculator unpopulated](img/tipcalculator_unpopulated.png)|![Tip calculator populated](img/tipcalculator_populated.png)|