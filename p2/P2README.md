# LIS4930 - Special Topics: Advanced Mobile Web Application Development

## Isaac Bass

### Project 2 Requirements:

1. Include splash screen (optional 10 additional points)
2. Insert at least five sample tasks
3. Test database class
4. Must add background color(s) or theme
5. Create and display launcher icon image

#### README.md file should include the following items:

* Course title, your name, and assignment requirements
* Screenshot of running application's Task List (optional splash screen: 10 additional points)

#### Assignment Screenshot:

*Screenshot of running application's Task List*

![Running application's Task List](img/p2.png)|