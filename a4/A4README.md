# LIS4930 - Special Topics: Advanced Mobile Web Application Development

## Isaac Bass

### Assignment 4 Requirements:

1. Include splash screen image, app title, intro text
2. Include appropriate images
3. Must use persistent data (SharedPreferences)
4. Widgets and images must be vertically and horizontally aligned
5. Must add background color(s) or theme
6. Create and display launcher icon image

#### README.md file should include the following items:

* Screenshot of running application's **splash screen**
* Screenshot of running application's **invalid screen** (with appropriate image)
* Screenshot of running application's **valid screen** (with appropriate image)

#### Assignment Screenshots:

|*Screenshot of running application's splash screen*|*Screenshot of running application's unpopulated screen*|
|-----------------------------------------------------------------|--------------------------------------------------------------|
|![Mortgage interest calculator splash screen](img/splash.png)|![Mortgage interest calculator unpopulated screen](img/unpopulated.png)|

|*Screenshot of running application's invalid screen*|*Screenshot of running application's valid screen*|
|-----------------------------------------------------------------|--------------------------------------------------------------|
|![Mortgage interest calculator invalid screen](img/invalid.png)|![Mortgage interest calculator valid screen](img/valid.png)|