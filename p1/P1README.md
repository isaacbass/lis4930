# LIS4930 - Special Topics: Advanced Mobile Web Application Development

## Isaac Bass

### Project 1 Requirements:

1. Include splash screen image, app title, and intro text
2. Include artists' images and media
3. Images and buttons must be vertically and horizontally aligned
4. Must add background color(s) or theme
5. Create and display launcher icon image

#### README.md file should include the following items:

* Course title, your name, and assignment requirements
* Screenshot of running application's splash screen
* Screenshot of running application's follow-up screen (with images and buttons)
* Screenshot of running application's pause and play user interfaces (with images and buttons)

#### Assignment Screenshots:

|*Screenshot of splash screen*|*Screenshot of follow-up screen*|
|-----------------------------------------------------------------|--------------------------------------------------------------|
|![Splash screen](img/splash.png)|![Follow-up screen](img/follow-up.png)|

|*Screenshot of pause user interface*|*Screenshot of play user interface*|
|-----------------------------------------------------------------|--------------------------------------------------------------|
|![Pause user interface](img/pause.png)|![Play user interface](img/play.png)|