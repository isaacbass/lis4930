# LIS4930 - Special Topics: Advanced Mobile Web Application Development

## Isaac Bass

### Assignment 3 Requirements:

1. Backward engineer currency converter application
2. Must include:
    * Field to enter U.S. dollar amount: 1 – 100,000
    * Toast notification if user enters out-of-range values
    * Radio buttons to convert to U.S. to Euros, Pesos, and Canadian currency (must be vertically and horizontally aligned)
    * Correct sign for euros, pesos, and Canadian dollars
    * Background color(s) or theme
    * Launcher icon image

#### README.md file should include the following items:

* Screenshot of running application's **unpopulated** user interface
* Screenshot of running application's **toast notification**
* Screenshot of running application's **converted currency** user interface

#### Assignment Screenshots:

|*Screenshot of running application's unpopulated user interface*|*Screenshot of running application's toast notification*|
|-----------------------------------------------------------------|--------------------------------------------------------------|
|![Currency converter unpopulated](img/currencyconverter_unpopulated.png)|![Currency converter toast](img/currencyconverter_toast.png)|

|*Screenshot of running application's converted currency user interface*||
|-----------------------------------------------------------------|--------------------------------------------------------------|
|![Currency converter populated](img/currencyconverter_populated.png)||