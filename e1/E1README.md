# LIS4930 - Special Topics: Advanced Mobile Web Application Development

## Isaac Bass

### Exam 1 Extra Credit Requirements:

1. Download R
2. Download RStudio
3. Go through Learn to Use R tutorial

#### README.md file should include the following items:

* R commands file
* R console screenshot
* Graph screenshots
* RStudio screenshot displaying:
    * R source-code
    * Console
    * Environment (or History)
    * Plots

#### Assignment Files

[commands.Rhistory](commands.Rhistory)

#### Assignment Screenshots:

|*Screenshot of RStudio*|*Screenshot of R console*|
|-----------------------------------------------------------------|--------------------------------------------------------------|
|![RStudio](img/rstudio.png)|![R console](img/console.png)|

|*Screenshots of graphs*||
|-----------------------------------------------------------------|--------------------------------------------------------------|
|![Graph 1](img/graph1.png)|![Graph 2](img/graph2.png)|
|![Graph 3](img/graph3.png)|![Graph 4](img/graph4.png)|
|![Graph 5](img/graph5.png)||