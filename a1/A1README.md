# LIS4930 - Special Topics: Advanced Mobile Web Application Development

## Isaac Bass

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1, 2)
4. Bitbucket repo links a) this assignment and b) the completed tutorials above (bitbucketstationlocations and my teamquotes)

#### README.md file should include the following items:

* Screenshot of running java Hello
* Screenshot of running Android Studio - My First App
* Screenshots of running Android Studio - Contacts App
* git commands w/ short descriptions

#### Git commands w/ short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git mv - Move or rename a file, a directory, or a symlink

>   This is a blockquote.
>
>   This is the second paragraph in the blockquote.

#### Assignment Screenshots:

*Screenshot of running java Hello:*

![Hello.java](img/hello.png)

|*Screenshot of Android Studio - My First App*|*Screenshot of Contacts App - Main Screen*|
|-----------------------------------------------|--------------------------------------------|
|![MyFirstApp](img/myfirstapp.png)|![Contacts app main screen](img/contacts_main.png)|

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:* [A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/isaacbass/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:* [A1 My Team Quotes Tutorial Link](https://bitbucket.org/isaacbass/myteamquotes/ "My Team Quotes Tutorial")