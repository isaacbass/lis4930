# LIS4930 - Special Topics: Advanced Mobile Web Application Development

## Isaac Bass

### **LIS4930 Requirements:**

*Course Work Links*

1. [A1README.md](a1/A1README.md)

    * Install JDK
    * Install Android Studio and create My First App and Contacts App
    * Provide screenshots of installations
    * Create Bitbucket repo
    * Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    * Provide git command descriptions

2. [A2README.md](a2/A2README.md)

    * Backward engineer tip calculator application
    * Must include:
        * Drop-down number for total number of guests (including yourself): 1-10
        * Drop-down menu for tip percentage (5% increments): 0-25
        * Must add background color(s) or theme

3. [A3README.md](a3/A3README.md)

    * Backward engineer currency converter application
    * Must include:
        * Field to enter U.S. dollar amount: 1 – 100,000
        * Toast notification if user enters out-of-range values
        * Radio buttons to convert to U.S. to Euros, Pesos, and Canadian currency (must be vertically and horizontally aligned)
        * Correct sign for euros, pesos, and Canadian dollars
        * Background color(s) or theme
        * Launcher icon image

4. [E1README.md](e1/E1README.md)

    * Download R
    * Download RStudio
    * Go through Learn to Use R tutorial

5. [P1README.md](p1/P1README.md)

    * Include splash screen image, app title, and intro text
    * Include artists' images and media
    * Images and buttons must be vertically and horizontally aligned
    * Must add background color(s) or theme
    * Create and display launcher icon image

6. [A4README.md](a4/A4README.md)

    * Include splash screen image, app title, intro text
    * Include appropriate images
    * Must use persistent data (SharedPreferences)
    * Widgets and images must be vertically and horizontally aligned
    * Must add background color(s) or theme
    * Create and display launcher icon image

7. [A5README.md](a5/A5README.md)

    * Include opening screen with app title and list of articles
    * Must find and use your own RSS feed
    * Must add background color(s) or theme
    * Create and display launcher icon image

8. [P2README.md](p2/P2README.md)

    * Include splash screen (optional 10 additional points)
    * Insert at least five sample tasks
    * Test database class
    * Must add background color(s) or theme
    * Create and display launcher icon image