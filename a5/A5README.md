# LIS4930 - Special Topics: Advanced Mobile Web Application Development

## Isaac Bass

### Assignment 5 Requirements:

1. Include opening screen with app title and list of articles
2. Must find and use your own RSS feed
3. Must add background color(s) or theme
4. Create and display launcher icon image

#### README.md file should include the following items:

* Screenshot of running application's **opening screen (list of articles - activity_items.xml**
* Screenshot of running application's **individual article (activity_item.xml)**
* Screenshot of running application's **default browser** (article link)

#### Assignment Screenshots:

|*Screenshot of opening screen (list of articles - activity_items.xml*|*Screenshot of individual article (activity_item.xml)*|
|-----------------------------------------------------------------|--------------------------------------------------------------|
|![News reader opening screen](img/items-activity.png)|![News reader individual article](img/item-activity.png)|

|*Screenshot of default browser*||
|-----------------------------------------------------------------|--------------------------------------------------------------|
|![News reader default browser](img/read-more.png)||